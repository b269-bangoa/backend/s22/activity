//console.log("Jiejie");

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. 

*/

function register(newUser) {
    //newUser = prompt("Enter new username: ");

   if(registeredUsers.includes(newUser) || !newUser){
       alert("Registration failed.\nUsername already exist!");
       return console.log(registeredUsers);
   } else {
        registeredUsers.push(newUser); 
        alert("Thank you for registering");
        return console.log(registeredUsers); 
   }
}

//register();

/*
    2. 

*/

function addFriend(newFriend) {
    //newFriend = prompt("Enter a user to add as friend: ");
    
    if(registeredUsers.includes(newFriend)) {
        friendsList.push(newFriend);
        alert("You have added " + newFriend + " as a friend.");
        return console.log("New Friends List:",friendsList);
    } else if(!newFriend) {
        return alert("User not found");
    }else {
        return alert("User not found"); 
    }
}

//addFriend();


/*
    3. 

*/


function displayFriendsList() {
    if(!friendsList.length) {
        return alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.forEach(function(friendList) {
            console.log(friendList);
        })
    }
}

//displayFriendsList();

/*
    4. 

*/

function displayAmountOfFriends() {
    if(!friendsList.length) {
        return alert("You currently have 0 friends. Add one first.");
    } else {
        return console.log("You currently have " + friendsList.length + " friends.");
    }
}

//displayAmountOfFriends();


/*
    5.

*/

function deleteRecentFriend() {
    if(!friendsList.length) {
        return alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.pop();
        return console.log("Friend list: " ,friendsList);
    }
}

//deleteRecentFriend();
//console.log("New Friends list:",friendsList);


/*
    Stretch Goal:

*/

function deleteAnyFriend(index) {
    //index = prompt("Enter friend's index: "); 

    if( index < friendsList.length) {
        friendsList.splice(index,1);
    return console.log("Friend list: ",friendsList);
} else {
    return alert("Error! No friend with index found");
}
}

//deleteAnyFriend();
//console.log(friendsList);






